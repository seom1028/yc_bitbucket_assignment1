About the Project (Assignment1)

You can use this project by double clicking on the index.html file in the folder.

This project is a web page aimed at changing photos by clicking two buttons. 
1.When user clicks the first button, the "dog" picture changes to the "carrot" picture. 
2.When user clicks the second button, the "carrot" picture changes to the "dog" picture.
This project complies with Accessibility for Ontarians with Disabilities Act (AODA). 


This source code from a previous assignment. 
Built With CSS,HTML and JQuery

Created by Yeseom Choe
